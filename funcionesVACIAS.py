﻿from configuracion import *
import re #para funcion filtro de eliminar caracteres especiales
import random
import math
import unicodedata
from extras import *

def lectura(archivo, letra, artistaYcancion):
     #se queda solo con los oraciones de cierta longitud y filtra tildes por e
    #Carga en una lista el archivo de letra random
    infoTotal = archivo.readlines()
    #Crea la lista artistaYcancion
    artycan=""
    #comienza la parte de separacion de la lista principal los valores para el armado de la lista artistaYcancion
    artycan=infoTotal[0]+";"
    concatenacion=""
    #ya se entiende que los finales de palabra eran los punto y coma se toma como valor de derivacion a la concatenacion
    for letras in artycan:
        if letras!=chr(59):
            concatenacion+=filtro(letras)
        else:
            artistaYcancion.append(concatenacion)
            concatenacion=""
    #Crea la lista Letra
    artistaYcancion=espacios(artistaYcancion)
    print("Respuestas: ",artistaYcancion)
    i=1
    infoTotalN=[]
    while i<len(infoTotal):
        if len(infoTotal[i]) > 45: # si la linea es larga crea otra lista y la reemplaza como la principal
           infoTotalN=rearmado(infoTotal)
           #las 3 lineas reiniciarian todo a cero, para volver a appendear todo otra vez.
           infoTotal=infoTotalN
           i=1
           letra=[]
        else:
            #si todo entra en la pantalla se guarda
           letra.append(filtro(infoTotal[i])) #si la lista mide normal, directamente se agrega a la lista
           i+=1
    return letra

#elimina los espacios vacios delante y detras de cada elemento en la lista lstrio y rstrip recortan espacio vacios no importa la cantidad de un lado y de el otro.
def espacios(lista):
    listaN=[]
    lineaN=""
    for linea in lista:
        if linea[0]==" " or linea[len(linea)-1]==" ":
            lineaN=linea.lstrip()
            lineaN=linea.rstrip()
            listaN.append(lineaN)
        else:
            listaN.append(linea)
    return listaN

#rearma la lista ingresante, reacomoda los texto largos
def rearmado(letraE):
    letraN=[]
    letraN.append(letraE[0])
    i=1
    while i < len(letraE):
        if len(letraE[i]) > 45:
            concatenacion=""
            linea=letraE[i]
            for j in range(len(linea)):
                if len(concatenacion)>23 and linea[j]==" ":#core de la funcion, separa lineas de mas de 23 caracteres. Cuando tiene mas de 23 espera el espacio vacio y entra
                    letraN.append(concatenacion)
                    concatenacion=""
                    i+=1
                elif j==len(linea)-1:
                    letraN.append(concatenacion)
                    concatenacion=""
                    i+=1
                else:
                    concatenacion+=linea[j]
        else:
             letraN.append(letraE[i])
             i+=1
    return letraN


#cambia caracteres especiales por " ". Elimina acentos
def filtro(linea):
    lineaN=""
    acent="áéíóú"
    for i in range(len(linea)): #Elimina los acentos
        if linea[i] in acent:
            if linea[i]=="á":
                lineaN+="a"
            elif linea[i]=="é":
                lineaN+="e"
            elif linea[i]=="í":
                lineaN+="i"
            elif linea[i]=="ó":
                lineaN+="o"
            else:
                lineaN+="u"
        else:
            lineaN+=linea[i]
    lineaF=re.sub("[@$&*_+\n-]", " ", lineaN) #elimina los caracteres y los reemplaza por " "
    return lineaF

def seleccion(letra):#elige uno al azar, devuelve ese y el siguiente
    listaL=[]
    i=0
    while i < 1:
        azarLinea=random.randint(0,len(letra)-2)
        if letra[azarLinea]!="" and letra[azarLinea+1]!="" and letra[azarLinea]!=" " and letra[azarLinea+1]!=" " and letra[azarLinea]!=letra[azarLinea+1]:#no deja que las lineas esten vacias o sean iguales
            listaL.append(letra[azarLinea])
            listaL.append(letra[azarLinea+1])
            i=1
    nLinea()
    return listaL
#funcion puntos, el valor degativo solo se desarrolla por pedido de TP, pero en la funcion prinsipal.py no se le da importancia ya que el restar no es logico dentro
#de lo que es un juego, quizas podria ser planteado como un modo hard de juego, pero el tiempo no alcanza para lograrlo(por ahora)
def puntos(n):
    valor=0
    if n==1 or n==0:
        segB1()
        valor=2
    elif n>1:
        segB2()
        valor=valor+2**n
    else:
        segM()
        valor=-2
    return valor


def esCorrecta(palabraUsuario, artistaYcancion, correctas):
    #chequea que sea correcta, que pertenece solo a la frase siguiente. Devuelve puntaje segun seguidilla
    correct=False
    puntitos=0
    lineaN=""
    for palabras in artistaYcancion:
        lineaN=palabras.lower()#corrije error de espacios y tamaños de letra
        lineaN=lineaN.rstrip()
        lineaN=lineaN.lstrip()
        if palabraUsuario == lineaN:
            correct=True
    if correct==True:
        puntitos=puntos(correctas)
    else:
        puntitos=puntos(-1)
    return puntitos

