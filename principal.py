#! /usr/bin/env python
import os, random, sys, math

import pygame
from pygame.locals import *
from configuracion import *
from extras import *

from funcionesVACIAS import *

#Funcion principal
def main():


        #Centrar la ventana y despues inicializar pygame
        os.environ["SDL_VIDEO_CENTERED"] = "1"
        pygame.init()
        #pygame.mixer.init()

        #Preparar la ventana
        pygame.display.set_caption("Cancionero...")
        screen = pygame.display.set_mode((ANCHO, ALTO))
        #definimos funciones


        #tiempo total del juego
        gameClock = pygame.time.Clock()
        totaltime = 0
        TIEMPO_MAX2 = TIEMPO_MAX + pygame.time.get_ticks()/1000 #variable para reinicio del juego
        segundos = TIEMPO_MAX2
        fps = FPS_inicial
        artistaYcancion=[]
        puntos = 0
        palabraUsuario = ""
        tamannoVariable=10
        letra=[]
        sumar=0
        puntos2=0
        correctas=0
        correctasAyuda=0
        elegidos= []
        VARIABLE_TAM=90
        acerto = False

        #Comienzo de Pantalla de Lanzamiento
        lanzamiento(screen)

        #Seleccion modo juego
        fps=introSeleccion(screen)

        #animacion salida
        TIEMPO_MAX2=salidaSeleccion(screen,TIEMPO_MAX2)


        #MODO DE JUEGO 1
        if fps==10:#cuando el modo 1 de juego es seleccionado la variable FPS toma valor 10 y carga
            #elige una cancion de todas las disponibles
            azar=random.randrange(1,N+1)
            elegidos.append(azar) #la agrega a la lista de los ya elegidos
            archivo= open(".\\letras\\"+str(azar)+".txt","r", encoding='utf-8') # abre el archivo elegido con unicode.
            #lectura del archivo y filtrado de caracteres especiales, la primer linea es el artista y cancion
            letra = lectura(archivo, letra, artistaYcancion)
            #elige una linea al azar y su siguiente
            lista=seleccion(letra)
            ayuda = "Cancionero"

        while segundos > fps/1000 and fps== 10:
            dibujar(screen, palabraUsuario, lista, puntos, segundos, correctasAyuda)#si el tiempo es 0 ejecuta sonido
            if segundos<1:
                pygame.mixer.music.stop()#detiene sonidos
            #muestra el nombre
            # 1 frame cada 1/fps segundos
            gameClock.tick(fps)
            totaltime += gameClock.get_time()

            if True:
            	fps = 10

            #Buscar la tecla apretada del modulo de eventos de pygame
            for e in pygame.event.get():
                #QUIT es apretar la X en la ventana
                if e.type == QUIT:
                    pygame.quit()
                    return()

                #Ver si fue apretada alguna tecla
                if e.type == pygame.KEYDOWN:
                    letraApretada = dameLetraApretada(e.key)
                    if len(palabraUsuario)<=38:
                        palabraUsuario += letraApretada
                    if e.key == K_BACKSPACE:
                        palabraUsuario = palabraUsuario[0:len(palabraUsuario)-1]
                    if e.key == K_RETURN:
                        #chequea si el usuario escribe algo, sino ejecuta burla
                        if palabraUsuario == "":
                            burla(artistaYcancion,screen)
                        #chequea si es correcta y suma o resta puntos
                        sumar=esCorrecta(palabraUsuario, artistaYcancion, correctas)
                        if sumar>=0:
                            puntos+=sumar
                        if sumar>0:
                            correctas=correctas+1
                            correctasAyuda=correctasAyuda+1
                        else:
                            correctas=0
                        if len(elegidos)==N:
                                elegidos=[]
                                #masDeUnaVuelta = True
                        dibujar(screen, palabraUsuario, lista, puntos, segundos, correctasAyuda)

                        azar=random.randrange(1,N+1)
                        while(azar in elegidos):
                            azar=random.randrange(1,N+1)

                        elegidos.append(azar)

                        #if masDeUnaVuelta == True:
                            #ayuda = artistaYcancion[0]


                        archivo= open(".\\letras\\"+str(azar)+".txt","r", encoding='utf-8')
                        palabraUsuario = ""
                        #lectura del archivo y filtrado de caracteres especiales
                        artistaYcancion=[]
                        letra=[]
                        letra=lectura(archivo, letra, artistaYcancion)
                        #elige una linea al azar y su siguiente
                        lista=seleccion(letra)
                    if e.key==1073741882 and correctasAyuda>=3:#cuando presiona F1 ejecuta la ayuda sonora
                        correctasAyuda-=3
                        ejecutarAyudaS(azar)

            segundos = TIEMPO_MAX2 - pygame.time.get_ticks()/1000

            #Limpiar pantalla anterior
            screen.fill(COLOR_FONDO)

            #Dibujar de nuevo todo
            dibujar(screen, palabraUsuario, lista, puntos, segundos, correctasAyuda)

            pygame.display.flip()
        #MODO DE JUEGO 2
        if fps >=20:#cuando el modo 2 de juego es seleccionado la variable FPS toma valor 20
            cartuchos=[]
            tiraDeCartuchos=[]
            #elige una cancion de todas las disponibles
            azar=random.randrange(1,N+1)
            elegidos.append(azar) #la agrega a la lista de los ya elegidos
            archivo= open(".\\letras\\"+str(azar)+".txt","r", encoding='utf-8') # abre el archivo elegido con unicode.
            #lectura del archivo y filtrado de caracteres especiales, la primer linea es el artista y cancion
            letra = lectura(archivo, letra, artistaYcancion)
            tiraDeCartuchos=artilleria(letra)
        while segundos > fps/1000 and fps>= 20:
            dibujar2(screen, palabraUsuario, puntos, segundos)
            #si el tiempo es 0 ejecuta sonido
            if segundos<1:
                pygame.mixer.music.stop()#detiene sonidos
            #muestra el nombre
            # 1 frame cada 1/fps segundos
            gameClock.tick(fps)
            totaltime += gameClock.get_time()

            #Buscar la tecla apretada del modulo de eventos de pygame
            for e in pygame.event.get():
                #QUIT es apretar la X en la ventana
                if e.type == QUIT:
                    pygame.quit()
                    return()
                if len(tiraDeCartuchos)==8:
                    segundos=0

                #Ver si fue apretada alguna tecla
                if e.type == pygame.KEYDOWN:
                    letraApretada = dameLetraApretada(e.key)
                    palabraUsuario=letraApretada
                    if len(palabraUsuario)<=1:
                        acerto=eliminarLetraLista(palabraUsuario)#la palabra usuario es llevada a la funcion la misma elimina la letra y devuelve booleano si acerto o no
                        #chequea si es correcta y suma o resta puntos
                        sumar=esCorrecta2(acerto,correctas)#entra con el booleano de eliminarLetraLista y la variable correctas que es la seguidilla de aciertos
                        if sumar>=0:
                            puntos2+=sumar
                        if sumar>0:
                            correctas=correctas+1
                        else:
                            correctas=0
                                #masDeUnaVuelta = True
                        puntos=int(puntos2)
                        dibujar2(screen, palabraUsuario, puntos, segundos)

            segundos = TIEMPO_MAX2 - pygame.time.get_ticks()/1000

            #Limpiar pantalla anterior
            screen.fill(COLOR_FONDO)

            #Dibujar de nuevo todo
            dibujar2(screen, palabraUsuario, puntos, segundos)

            pygame.display.flip()
        #cuando termina el tiempo, va a la pantalla de SCORE y luego da la posibilidad para comenzar otra vez.
        if segundos<1:
            j=0
            #Arma la nueva posicion del jugador y carga las demas
            listaJugadores = armadoLista(puntos,screen)#funcion que revisa los scores y habilita al usuario a dejar su nombre si el puntaje es mas alto
            while j < 1:

                    #detiene sonidos
                    pygame.mixer.music.stop()

                    #limpia pantalla anterior
                    screen.fill(COLOR_FONDO)

                    #carga de fondo de pantalla animado
                    global contadorDibujar
                    global bajandoDibu


                    if int(contadorDibujar) == 24:
                        contadorDibujar=0
                    elif int(contadorDibujar) < 24:
                        contadorDibujar=int(contadorDibujar)+1
                    contadorDibujar='{0:02d}'.format(contadorDibujar)

                    background_image = pygame.image.load("./img/pantallaFinal"+str(contadorDibujar)+".png").convert()
                    screen.blit(background_image, [0, 0])

                    #tamaños de letra
                    defaultFontGrande= pygame.font.Font( "./fonts/Fairy_Tales.otf", TAMANNO_LETRA_GRANDE)
                    defaultFont= pygame.font.Font( "./fonts/Fairy_Tales.otf", TAMANNO_LETRA)
                    #Lineas Horizontales
                    pygame.draw.line(screen, (255,255,255), (341, ALTO-450) , (682, ALTO-450), 5)
                    pygame.draw.line(screen, (255,255,255), (341, ALTO-550) , (682, ALTO-550), 5)
                    pygame.draw.line(screen, (255,255,255), (341, ALTO-80) , (682, ALTO-80), 5)
                    #muestra titulo
                    scoreT="Los 10 Mejores"
                    screen.blit(defaultFontGrande.render(scoreT, 1, COLOR_LETRAS), (390,80))
                    #muestra los 10 mejores
                    top=""
                    contadorUP=130
                    listaJugadores.sort(reverse=True)
                    for i in range(len(listaJugadores)):
                        if i < 10:
                            top=str(i+1) + "." +"    "+ listaJugadores[i][5:8].capitalize()+"        " + listaJugadores[i][0:4]#separa los valores de la lista de jugadores
                            contadorUP=contadorUP+35
                            screen.blit(defaultFont.render(top, 1, COLOR_LETRAS), (400,contadorUP))
                    defaultFont= pygame.font.Font( pygame.font.get_default_font(), TAMANNO_LETRA)
                    #palabra mensaje volver a jugar
                    mensaje="Presiona ENTER para volver a jugar"
                    screen.blit(defaultFont.render(mensaje, 1, COLOR_TEXTO), (220, 550))
                    #acciona pantalla
                    pygame.display.flip()
                    #Buscar la tecla apretada del modulo de eventos de pygame
                    for e in pygame.event.get():
                        #Ver si fue apretada alguna tecla
                        if e.type == pygame.KEYDOWN:
                            if e.key == K_RETURN:
                                j=1
                                screen.fill(COLOR_FONDO)
                                #Dibujar de nuevo todo
                                pygame.display.flip()
                                main()
                        if e.type == QUIT:
                            pygame.quit()
                            return
        while 1:
            #Esperar el QUIT del usuario
            for e in pygame.event.get():
                if e.type == QUIT:
                    pygame.quit()
                    return


        archivo.close()

#Programa Principal ejecuta Main
if __name__ == "__main__":
    main()
