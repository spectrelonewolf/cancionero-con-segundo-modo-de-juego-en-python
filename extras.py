import pygame
from pygame.locals import *
from configuracion import *
import time
import re
import random

def dameLetraApretada(key):
    if key == K_a:
        return("a")
    elif key == K_b:
        return("b")
    elif key == K_c:
        return("c")
    elif key == K_d:
        return("d")
    elif key == K_e:
        return("e")
    elif key == K_f:
        return("f")
    elif key == K_g:
        return("g")
    elif key == K_h:
        return("h")
    elif key == K_i:
        return("i")
    elif key == K_j:
        return("j")
    elif key == K_k:
        return("k")
    elif key == K_l:
        return("l")
    elif key == K_m:
        return("m")
    elif key == K_n:
        return("n")
    elif key == 241:
        return("ñ")
    elif key == K_o:
        return("o")
    elif key == K_p:
        return("p")
    elif key == K_q:
        return("q")
    elif key == K_r:
        return("r")
    elif key == K_s:
        return("s")
    elif key == K_t:
        return("t")
    elif key == K_u:
        return("u")
    elif key == K_v:
        return("v")
    elif key == K_w:
        return("w")
    elif key == K_x:
        return("x")
    elif key == K_y:
        return("y")
    elif key == K_z:
        return("z")
    elif key == K_0:
        return("0")
    elif key == K_1:
        return("1")
    elif key == K_2:
        return("2")
    elif key == K_3:
        return("3")
    elif key == K_4:
        return("4")
    elif key == K_5:
        return("5")
    elif key == K_6:
        return("6")
    elif key == K_7:
        return("7")
    elif key == K_8:
        return("8")
    elif key == K_9:
        return("9")
    elif key == K_KP_MINUS:
        return("-")
    elif key == K_SPACE:
       return(" ")
    else:
        return("")

#Lanzamiento del juego
def lanzamiento(screen):
    lanzamiento=0
    bajando=1
    contadorLanzamiento=100
    i=0
    sonEntra()
    while i < 1:
            time.sleep(0.02)#FPS de pantalla principal
            #limpia pantalla anterior
            screen.fill(COLOR_FONDO)

            #carga de fondo de pantalla
            #Antes y despues de presionar ENTER en pantalla principal
            if contadorLanzamiento == 149:
                sonSale()
                bajando=0
            elif contadorLanzamiento < 149 and bajando==1:
                contadorLanzamiento+=1
            if contadorLanzamiento == 125:
                sonSale()
            if contadorLanzamiento == 100:
                sonEntra()
                bajando=1
                if lanzamiento==1:# Detiene el while y continua sin aportar nada
                    i=1
            elif contadorLanzamiento > 100 and bajando==0:
                contadorLanzamiento-=1
            background_image = pygame.image.load("./img/Comp"+" "+str(contadorLanzamiento)+".png").convert()
            screen.blit(background_image, [0, 0])


            #Buscar la tecla apretada del modulo de eventos de pygame
            for e in pygame.event.get():
                #Ver si fue apretada alguna tecla
                if e.type == pygame.KEYDOWN:
                    if e.key == K_RETURN:
                        sonEnter()
                        lanzamiento=1
                if e.type == QUIT:
                    pygame.quit()
                    return
            pygame.display.flip()


#INTRO MODO DE JUEGO
def introSeleccion(screen):
    global fps
    lanzamiento=0
    bajando=1
    intro=1
    contadorLanzamiento=00
    i=0
    sonEntra()
    while i < 1:
            time.sleep(0.02)#FPS de pantalla principal
            #limpia pantalla anterior
            screen.fill(COLOR_FONDO)

            #carga animacion de fondo de pantalla INTRO
            if int(contadorLanzamiento) < 19 and intro==1 :
                contadorLanzamiento=int(contadorLanzamiento)+1
                contadorLanzamiento='{0:02d}'.format(contadorLanzamiento)
                background_image = pygame.image.load("./img/etapaMediaPresent"+str(contadorLanzamiento)+".png").convert()
            if int(contadorLanzamiento) == 19 and intro==1:
                intro=0
                bajando=1
                contadorLanzamiento=0
            #carga animacion de fondo de pantalla CONTINUACION de INTRO pero bucleada
            elif int(contadorLanzamiento) < 49 and bajando==0 and intro==0:
                contadorLanzamiento=int(contadorLanzamiento)+1
                contadorLanzamiento='{0:02d}'.format(contadorLanzamiento)
                background_image = pygame.image.load("./img/etapaSelect"+str(contadorLanzamiento)+".png").convert()
            elif int(contadorLanzamiento) == 49 and intro==0:
                bajando=1
            if int(contadorLanzamiento) > 0 and bajando==1 and intro==0:
                contadorLanzamiento=int(contadorLanzamiento)-1
                contadorLanzamiento='{0:02d}'.format(contadorLanzamiento)
                background_image = pygame.image.load("./img/etapaSelect"+str(contadorLanzamiento)+".png").convert()
            elif int(contadorLanzamiento) == 0 and intro==0:
                if lanzamiento==1:
                    i=1
                bajando=0
            screen.blit(background_image, [0, 0])

            #Buscar la tecla apretada del modulo de eventos de pygame
            for e in pygame.event.get():
                #Ver si fue apretada alguna tecla
                if e.type == pygame.KEYDOWN:
                    #si se selecciona uno u otro cambia FPS a 10 o 20
                    if e.key == K_1:
                        lanzamiento=1
                        sonEnter()
                        fps=10
                    if e.key == K_2:
                        lanzamiento=1
                        sonEnter()#sinido de Enter
                        fps=20
                if e.type == QUIT:
                    pygame.quit()
                    return
            pygame.display.flip()
    return fps

#animacion salida
def salidaSeleccion(screen,TIEMPO_MAX2):
    lanzamiento=1
    contadorLanzamiento=100
    i=0
    lineaSup=61
    lineaInf=529
    sonEntra()
    while i < 1:
            time.sleep(0.005)#FPS de pantalla principal
            #limpia pantalla anterior
            screen.fill(COLOR_FONDO)

            #Animacion de salida de introSelecion() segun los FPS marcados(modo juego) ejecuta animacion de lineas de colores
            if int(contadorLanzamiento) < 152:
                contadorLanzamiento=int(contadorLanzamiento)+1
                lineaSup+=3
                lineaInf-=3
            elif int(contadorLanzamiento) == 152:
                bajando=1
                i=1
                lanzamiento=1
            background_image = pygame.image.load("./img/Pre-comp"+" "+str(contadorLanzamiento)+".png").convert()
            screen.blit(background_image, [0, 0])

            #lineas de colores con animacion de colores Random
            if lanzamiento==1 and fps==10:
                colorLineaR=random.randint(0,255)
                colorLineaG=random.randint(0,255)
                colorLineaB=random.randint(0,255)

                pygame.draw.line(screen, (colorLineaR,colorLineaG,colorLineaB), (0, lineaSup) , (ANCHO, lineaSup), 5)
                pygame.draw.line(screen, (colorLineaR,colorLineaG,colorLineaB), (0, lineaInf) , (ANCHO, lineaInf), 5)

            pygame.display.flip()
    TIEMPO_MAX2 = TIEMPO_MAX + pygame.time.get_ticks()/1000 #variable para reinicio del tiempo de juego
    return TIEMPO_MAX2


#MODO DE JUEGO 1
contadorDibujar=1
bajandoDibu=0

def dibujar(screen, palabraUsuario, lista, puntos, segundos, correctasAyuda):
    #limpia pantalla anterior
    screen.fill(COLOR_FONDO)
    #carga de fondo de pantalla animado
    global contadorDibujar
    global bajandoDibu
    if int(contadorDibujar) == 1:
        bajandoDibu=1
    elif int(contadorDibujar) == 49:
        bajandoDibu=0

    if int(contadorDibujar) > 0 and bajandoDibu==0:
        contadorDibujar=int(contadorDibujar)-1

    elif int(contadorDibujar) < 49 and bajandoDibu==1:
        contadorDibujar=int(contadorDibujar)+1

    contadorDibujar='{0:02d}'.format(contadorDibujar)

    background_image = pygame.image.load("./img/fondo principal"+str(contadorDibujar)+".png").convert()
    screen.blit(background_image, [0, 0])

    #tipografia de puntos tiempo e ingreso de usuario
    defaultFont= pygame.font.Font( "./fonts/Fairy_Tales.otf", TAMANNO_LETRA)

    #tipografia de las letras de cancion
    defaultFontGrande= pygame.font.Font( "./fonts/Fairy_Tales.otf", TAMANNO_LETRA_GRANDE)

    #muestra el puntaje
    screen.blit(defaultFont.render("Puntos: " + str(puntos), 1, COLOR_TEXTO), (780, 8))

    #Muestra si esta habilitada la ayuda sonora o no
    tituloAyudaS="Presiona F1 para una pista"
    if correctasAyuda>=3:
        screen.blit(defaultFont.render(tituloAyudaS, 1, COLOR_TEXTO), (640, 550))

    #muestra los segundos y puede cambiar de color con el tiempo
    if(segundos<15):
        ren = defaultFont.render("Tiempo: " + str(int(segundos)), 1, COLOR_TIEMPO_FINAL)
    else:
        ren = defaultFont.render("Tiempo: " + str(int(segundos)), 1, COLOR_TEXTO)
    screen.blit(ren, (10, 8))

    #TITULO
    #titulo="Cancionero!"
    #defaultFontTitulo= pygame.font.Font( "./fonts/Deadhead_Script.otf", VARIABLE_TAM)
    #screen.blit(defaultFontTitulo.render(titulo, 1, (222,50,175)), (ANCHO//2-len(titulo)*120//7,ALTO/7))

    #linea horizontal de puntos y segundos
    pygame.draw.line(screen, (102,153,153), (0, 40) , (ANCHO, 40), 4)

    #Linea Horizontal de palabra usuario
    pygame.draw.line(screen, (102,153,153), (0, ALTO-70) , (ANCHO, ALTO-70), 5)

    #lineas de colores animadas colores Random para la letra de la cancion
    colorLineaR=random.randint(0,255)
    colorLineaG=random.randint(0,255)
    colorLineaB=random.randint(0,255)
    pygame.draw.line(screen, (colorLineaR,colorLineaG,colorLineaB), (0, 220) , (ANCHO, 220), 5)
    pygame.draw.line(screen, (colorLineaR,colorLineaG,colorLineaB), (0, 370) , (ANCHO, 370), 5)

    #muestra las 2 lineas de la cancion
    screen.blit(defaultFontGrande.render(lista[0], 1, COLOR_LETRAS), (ANCHO//2-len(lista[0])*TAMANNO_LETRA_GRANDE//6,240))
    screen.blit(defaultFontGrande.render(lista[1], 1, COLOR_LETRAS), (ANCHO//2-len(lista[1])*TAMANNO_LETRA_GRANDE//6,310))

    #muestra lo que escribe el jugador
    screen.blit(defaultFont.render(palabraUsuario, 1, COLOR_TEXTO), (160, 550))


#Ejecuta la burla
def burla(artistaYcancion, screen):
    #detiene sonidos
    pygame.mixer.music.stop()
    #Limpiar pantalla anterior
    screen.fill(COLOR_FONDO)
    #tamaños de letra
    defaultFontGrande= pygame.font.Font( "./fonts/Lickety Split 2015.ttf", TAMANNO_LETRA_GRANDE)
    defaultFont= pygame.font.Font( "./fonts/Lickety Split 2015.ttf", TAMANNO_LETRA)
    #Lineas Horizontales
    pygame.draw.line(screen, (255,255,255), (0, ALTO-320) , (ANCHO, ALTO-320), 5)
    pygame.draw.line(screen, (255,255,255), (0, ALTO-380) , (ANCHO, ALTO-380), 5)
    pygame.draw.line(screen, (255,255,255), (0, ALTO-480) , (ANCHO, ALTO-480), 5)
    #muestra artista
    screen.blit(defaultFontGrande.render(artistaYcancion[0].upper(), 1, COLOR_LETRAS), (ANCHO//2-len(artistaYcancion[0])*TAMANNO_LETRA_GRANDE//3,(TAMANNO_LETRA_GRANDE)*3))
    #muestra cancion
    screen.blit(defaultFont.render(artistaYcancion[len(artistaYcancion)-1].capitalize(), 1, COLOR_LETRAS), (ANCHO//2-len(artistaYcancion[len(artistaYcancion)-1])*TAMANNO_LETRA_GRANDE//6,(TAMANNO_LETRA_GRANDE)*5))
    #ejecuta sonido de burla
    burlaS()
    #ejecuta visualizacion de todo lo anterior
    pygame.display.flip()
    #se toma un tiempo para volver a la funcion principal
    time.sleep(3)


#armado de lista de jugadores, pidea datos a jugador y anota puntos mejores 10 ordenados
def armadoLista(puntos,screen):
    #abre el archivo de jugadores y puntajes, los guarda, los acomoda, les elimina el salto de linea y las guarda en una lista nueva
    archivo=open("scoreJugadores.txt","r", encoding='utf-8') # abre el archivo elegido con unicode.
    listaTotal = archivo.readlines()
    listaTotal.sort(key = str)#acomoda la lista por orden decendente
    listaTotalN=[]
    palabraUsuario=""
    for elemento in listaTotal:
        lineaN=re.sub("[\n]", "", elemento)#elimina salto de linea
        listaTotalN.append(lineaN)
    lineaLimpia=int(listaTotalN[0][0:4])#separa el valor numerico de la primer posicion la cual es la menor del top 10 de la cadena de texto
    if int(lineaLimpia)<puntos:
        i=0
        while i < 1:

                #limpia pantalla
                screen.fill(COLOR_FONDO)
                #tamaño de letra y tipo
                defaultFont= pygame.font.Font( pygame.font.get_default_font(), TAMANNO_LETRA)
                #Linea Horizontal
                pygame.draw.line(screen, (255,255,255), (0, ALTO-70) , (ANCHO, ALTO-70), 5)
                #palabra mensaje ingreso de datos
                mensaje="Ingrese sus Iniciales"
                screen.blit(defaultFont.render(mensaje, 1, COLOR_TEXTO), (190, 480))
                #palabra de usuario
                screen.blit(defaultFont.render(palabraUsuario, 1, COLOR_TEXTO), (190, 540))
                #acciona pantalla

                #Buscar la tecla apretada del modulo de eventos de pygame
                for e in pygame.event.get():
                    #Ver si fue apretada alguna tecla
                    if e.type == pygame.KEYDOWN:
                        letraApretada = dameLetraApretada(e.key)
                        if len(palabraUsuario)<3:
                            palabraUsuario += letraApretada.upper()
                        if e.key == K_BACKSPACE:
                            palabraUsuario = palabraUsuario[0:len(palabraUsuario)-1]
                        if e.key == K_RETURN and palabraUsuario!="" and palabraUsuario!=" " and len(palabraUsuario)==3:#si se cambia el len ==3, se pueden guardar nombres mas largos
                            listaTotalN.append(str('{0:04d}'.format(puntos))+" "+palabraUsuario) #le coloca 00 adelante o menos
                            listaTotalN.sort(key=str, reverse=True)
                            i=1
                            #guardar lista en archivo TXT
                            guardar = open("scoreJugadores.txt","w")
                            for i in range(10):
                               a=guardar.write(listaTotalN[i]+"\n")
                            guardar.close()
                pygame.display.flip()
    return listaTotalN


#Seccion que Reproduce Sonidos

def ejecutarAyudaS(azar):
    if azar==1:
        playSonido("./sonidos/pistas/1_1.mp3")
    if azar==2:
        playSonido("./sonidos/pistas/2_1.mp3")
    if azar==3:
        playSonido("./sonidos/pistas/3_1.mp3")
    if azar==4:
        playSonido("./sonidos/pistas/4_1.mp3")
    if azar==5:
        playSonido("./sonidos/pistas/5_1.mp3")
    if azar==6:
        playSonido("./sonidos/pistas/6_1.mp3")
    if azar==7:
        playSonido("./sonidos/pistas/7_1.mp3")
    if azar==8:
        playSonido("./sonidos/pistas/8_1.mp3")
    if azar==9:
        playSonido("./sonidos/pistas/9_1.mp3")

#entra vision
def sonEntra():
    pygame.mixer.music.load("./sonidos/Positive Outv6.mp3")
    pygame.mixer.music.play()
#sale vision
def sonSale():
    pygame.mixer.music.load("./sonidos/Positive Outv5.mp3")
    pygame.mixer.music.play()
#dio enter
def sonEnter():
    pygame.mixer.music.load("./sonidos/Positive Game Sound 18.mp3")
    pygame.mixer.music.play()
#acerto
def segB1():
    playSonido("./sonidos/Equip Gained Hit 1.mp3")
#acerto seguidilla
def segB2():
    playSonido("./sonidos/Equip Gained Hit 2.mp3")
#erro
def segM():
    playSonido("./sonidos/Wrong Answer Game Over 1.mp3")
#termino tiempo
def endGame():
    playSonido("./sonidos/Game Over 10.mp3")
#burla
def burlaS():
    playSonido("./sonidos/failed.wav")
#carga nueva linea
def nLinea():
    pygame.mixer.music.load("./sonidos/Airy Gas Unlock Bonus 1.wav")
    pygame.mixer.music.play()

def playSonido(ruta):
                pygame.mixer.music.load(ruta)
                pygame.mixer.music.play()
                #time.sleep(1.5)

#MODO DE JUEGO 2
#crea la lista de palabras con la posicion en el centro de la pontalla con valores random dentro de los ejes X e Y fomados por los medios del Alto y Ancho de la pantalla
tiraDeCartuchos=[]
def artilleria(letra):
    caractSpec=".!¡()[],?¿:"
    concatenador=""
    cartuchos=[]
    for i in range(len(letra)):#Separa todas las palabras en bloques sin espacios y sin caracteres especiales
        linea=re.sub("[\n]", "", letra[i])
        elementos=linea+" "
        for j in range(len(elementos)):
            if elementos[j]!="" and not elementos[j] in caractSpec and elementos[j]!=" ":#resuelve que solo entren letras, palabras
                concatenador=concatenador+elementos[j]
            elif len(concatenador)!=0:#luego de concatenar "ALGO" entra y suma a cartuchos
                cartuchos.append(concatenador.lower())
                concatenador=""
    #codigo generado para sumarle posicion tamaño e indice a la palabra, todo en una cadena de texto
    global tiraDeCartuchos
    tiraDeCartuchos=[]
    concatenador=""
    for i in range(len(cartuchos)):#Armado de la tira de cartuchos
        posicionX='{0:04d}'.format(random.randint(462,562))
        posicionY=random.randint(250,350)
        concatenador=str('{0:03d}'.format(i))+str(posicionX)+str(posicionY)+"020"+cartuchos[i]#3 INDICE +4 POSICION X +3 POSICION Y +3 TAMAÑO + PALABRA / los numeros son los digitos empleados para cada una de las supuestas variables representadas en cadena de texto
        tiraDeCartuchos.append(concatenador)
    z=0
    contTotales=0
    indiceTotalCartuchos=-1
    return tiraDeCartuchos

#Pantalla Del modo de juego 2
contTotales=0
indiceTotalCartuchos=0
#Se encarga de mostrar las letras, animaciones y ejecucion de cada una de las partes necesarias para que el modo de juego 2 funcione
def dibujar2(screen, palabraUsuario, puntos, segundos):
    global contadorDibujar
    global bajandoDibu
    global contTotales
    global indiceTotalCartuchos


    #limpia pantalla anterior
    screen.fill(COLOR_FONDO)
    #carga de fondo de pantalla animada
    #hace el enganche de animacion con salidaSeleccion()
    if int(contadorDibujar) == 19 and bajandoDibu==0:
        bajandoDibu=1
        contadorDibujar=0

    elif int(contadorDibujar) < 19 and bajandoDibu==0:
        contadorDibujar=int(contadorDibujar)+1
        contadorDibujar='{0:02d}'.format(contadorDibujar)
        background_image2 = pygame.image.load("./img/primeraAparicion"+str(contadorDibujar)+".png").convert()
        screen.blit(background_image2, [0, 0])
    #animacion de fondo fija bucleada
    if int(contadorDibujar) == 33 and bajandoDibu==1:
        contadorDibujar=0
        contadorDibujar='{0:02d}'.format(contadorDibujar)
        background_image = pygame.image.load("./img/lluviaLetras"+str(contadorDibujar)+".png").convert()
        screen.blit(background_image, [0, 0])

    elif int(contadorDibujar) < 33 and bajandoDibu==1:
        contadorDibujar=int(contadorDibujar)+1
        contadorDibujar='{0:02d}'.format(contadorDibujar)
        background_image = pygame.image.load("./img/lluviaLetras"+str(contadorDibujar)+".png").convert()
        screen.blit(background_image, [0, 0])

    #tipografia de puntos tiempo e ingreso de usuario
    defaultFont= pygame.font.Font( "./fonts/Fairy_Tales.otf", TAMANNO_LETRA)

    #muestra el puntaje
    screen.blit(defaultFont.render("Puntos: " + str(puntos), 1, COLOR_TEXTO), (780, 8))

    #muestra los segundos y puede cambiar de color con el tiempo
    if(segundos<15):
        ren = defaultFont.render("Tiempo: " + str(int(segundos)), 1, COLOR_TIEMPO_FINAL)
    else:
        ren = defaultFont.render("Tiempo: " + str(int(segundos)), 1, COLOR_TEXTO)
    screen.blit(ren, (10, 8))

    #linea horizontal de puntos y segundos
    pygame.draw.line(screen, (102,153,153), (0, 40) , (ANCHO, 40), 4)

    #muestra las lineas de la cancion
    #tipografia de las letras de cancion
    if contTotales != 5 and len(tiraDeCartuchos)>5:
        contTotales+=1
    #bloque Core del juego el cual se encarga de ir ejecutando las funciones las cuales animan los textos en pantalla
    indiceTotalCartuchos=0
    #de aca hacia abajo hay 5 bloques los cuales cambian las variables etc, se encargan cada uno de tomar cada uno una palabra de la lista de palabras siempre los primeros 5
    #por cuestiones de tiempo no se mejoro este bloque ya que se necesita definir en orden creciente su eje Z o profundidad de cada bloque, ya que el juego lo necesita asi.
    if contTotales > -1:
        #ejemplo random de un indice CERO dentro de tiraDeCartuchos "0000470289020los"
        indice1=int(tiraDeCartuchos[indiceTotalCartuchos][0:3])#separa el indice al que pertenece la palabra en realidad del total de la lista en un comienzo, no se le dio una funcion por tiempo
        posicionX1=posicionXCart(int(tiraDeCartuchos[indiceTotalCartuchos][3:7]))#actualiza la posicion en el eje X, mediante la funcion
        posicionY1=posicionYCart(int(tiraDeCartuchos[indiceTotalCartuchos][7:10]))#actualiza la posicion en el eje Y, mediante la funcion
        tamannio1=tamannioPalabra(int(tiraDeCartuchos[indiceTotalCartuchos][10:13]))#actualiza tamiño de palabra, lo que da la sensacion de acercarse al usuario o desplazarse en el eje Z
        palabra1=tiraDeCartuchos[indiceTotalCartuchos][13:len(tiraDeCartuchos[indiceTotalCartuchos])]#separa la palabra de la cadena de texto

        COLOR_LETRAS=(220,0,0)

        defaultFontGrande1= pygame.font.Font( "./fonts/Fairy_Tales.otf", int(tamannio1))
        #muestra la linea cargada anteriormente
        screen.blit(defaultFontGrande1.render(palabra1, 1, COLOR_LETRAS), (posicionX1,posicionY1))
        #guarda los nuevos valores adquiridos en el bloque
        tiraDeCartuchos[indiceTotalCartuchos]=str('{0:03d}'.format(indice1))+str('{0:04d}'.format(posicionX1))+str('{0:03d}'.format(posicionY1))+str('{0:03d}'.format(tamannio1))+palabra1
        #si la letra llega al tamaño de 200 se elimina la palabra con sus datos de la lista principal
        if tamannio1>200:
                    del tiraDeCartuchos[indiceTotalCartuchos]
        #aumenta en uno el indice para el siguiente bloque
        indiceTotalCartuchos+=1


    if contTotales > 0:

        indice2=int(tiraDeCartuchos[indiceTotalCartuchos][0:3])
        posicionX2=posicionXCart(int(tiraDeCartuchos[indiceTotalCartuchos][3:7]))
        posicionY2=posicionYCart(int(tiraDeCartuchos[indiceTotalCartuchos][7:10]))
        tamannio2=tamannioPalabra(int(tiraDeCartuchos[indiceTotalCartuchos][10:13]))
        palabra2=tiraDeCartuchos[indiceTotalCartuchos][13:len(tiraDeCartuchos[indiceTotalCartuchos])]

        COLOR_LETRAS=(255,120,0)

        defaultFontGrande1= pygame.font.Font( "./fonts/Fairy_Tales.otf", int(tamannio2))

        screen.blit(defaultFontGrande1.render(palabra2, 1, COLOR_LETRAS), (posicionX2,posicionY2))

        tiraDeCartuchos[indiceTotalCartuchos]=str('{0:03d}'.format(indice2))+str('{0:04d}'.format(posicionX2))+str('{0:03d}'.format(posicionY2))+str('{0:03d}'.format(tamannio2))+palabra2

        if tamannio2>200:
                    del tiraDeCartuchos[indiceTotalCartuchos]

        indiceTotalCartuchos+=1

    if contTotales > 1:

        indice3=int(tiraDeCartuchos[indiceTotalCartuchos][0:3])
        posicionX3=posicionXCart(int(tiraDeCartuchos[indiceTotalCartuchos][3:7]))
        posicionY3=posicionYCart(int(tiraDeCartuchos[indiceTotalCartuchos][7:10]))
        tamannio3=tamannioPalabra(int(tiraDeCartuchos[indiceTotalCartuchos][10:13]))
        palabra3=tiraDeCartuchos[indiceTotalCartuchos][13:len(tiraDeCartuchos[indiceTotalCartuchos])]

        COLOR_LETRAS=(255,232,0)

        defaultFontGrande1= pygame.font.Font( "./fonts/Fairy_Tales.otf", int(tamannio3))

        screen.blit(defaultFontGrande1.render(palabra3, 1, COLOR_LETRAS), (posicionX3,posicionY3))

        tiraDeCartuchos[indiceTotalCartuchos]=str('{0:03d}'.format(indice3))+str('{0:04d}'.format(posicionX3))+str('{0:03d}'.format(posicionY3))+str('{0:03d}'.format(tamannio3))+palabra3

        if tamannio3>200:
                    del tiraDeCartuchos[indiceTotalCartuchos]

        indiceTotalCartuchos+=1

    if contTotales > 2:

        indice4=int(tiraDeCartuchos[indiceTotalCartuchos][0:3])
        posicionX4=posicionXCart(int(tiraDeCartuchos[indiceTotalCartuchos][3:7]))
        posicionY4=posicionYCart(int(tiraDeCartuchos[indiceTotalCartuchos][7:10]))
        tamannio4=tamannioPalabra(int(tiraDeCartuchos[indiceTotalCartuchos][10:13]))
        palabra4=tiraDeCartuchos[indiceTotalCartuchos][13:len(tiraDeCartuchos[indiceTotalCartuchos])]

        COLOR_LETRAS=(0,222,0)

        defaultFontGrande1= pygame.font.Font( "./fonts/Fairy_Tales.otf", int(tamannio4))

        screen.blit(defaultFontGrande1.render(palabra4, 1, COLOR_LETRAS), (posicionX4,posicionY4))

        tiraDeCartuchos[indiceTotalCartuchos]=str('{0:03d}'.format(indice4))+str('{0:04d}'.format(posicionX4))+str('{0:03d}'.format(posicionY4))+str('{0:03d}'.format(tamannio4))+palabra4

        if tamannio4>200:
                    del tiraDeCartuchos[indiceTotalCartuchos]

        indiceTotalCartuchos+=1

    if contTotales > 3:

        indice5=int(tiraDeCartuchos[indiceTotalCartuchos][0:3])
        posicionX5=posicionXCart(int(tiraDeCartuchos[indiceTotalCartuchos][3:7]))
        posicionY5=posicionYCart(int(tiraDeCartuchos[indiceTotalCartuchos][7:10]))
        tamannio5=tamannioPalabra(int(tiraDeCartuchos[indiceTotalCartuchos][10:13]))
        palabra5=tiraDeCartuchos[indiceTotalCartuchos][13:len(tiraDeCartuchos[indiceTotalCartuchos])]

        COLOR_LETRAS=(3,104,163)

        defaultFontGrande1= pygame.font.Font( "./fonts/Fairy_Tales.otf", int(tamannio5))

        screen.blit(defaultFontGrande1.render(palabra5, 1, COLOR_LETRAS), (posicionX5,posicionY5))

        tiraDeCartuchos[indiceTotalCartuchos]=str('{0:03d}'.format(indice5))+str('{0:04d}'.format(posicionX5))+str('{0:03d}'.format(posicionY5))+str('{0:03d}'.format(tamannio5))+palabra5

        if tamannio5>250:
                    del tiraDeCartuchos[indiceTotalCartuchos]

    pygame.display.flip()

#primera funcion del segundo modo de juego, es el core, o el sentido del juego, la eliminacion de letras de la palabra mostrada en pantalla
def eliminarLetraLista(palabraUsuario):
    global tiraDeCartuchos
    letra=palabraUsuario
    i=0
    #lineas que comprueban la letra ingresada solo al tocarla, pueden ser lanzadas una atras de otra sin restricciones de enter o tiempos.
    if letra in tiraDeCartuchos[0]:
        i=0
    elif letra in tiraDeCartuchos[1]:
        i=1
    elif letra in tiraDeCartuchos[2]:
        i=2
    elif letra in tiraDeCartuchos[3]:
        i=3
    elif letra in tiraDeCartuchos[4]:
        i=4
    else:
        return False
    #en caso de detectar en cualquiera de las 5 palabras puestas en pantalla la letra tocada, se encarga de liminarla, pero todas las letras, eso le pone un tinte mas dificial ya que lo que seria el data entry
    #debiera adelantarse a que si tengo la palabra PATATA si presiono A se eliminarian las 3  A y asi qeudadirian mas las consonantes y no seria escribir de inercia
    palabraEntera=tiraDeCartuchos[i][13:len(tiraDeCartuchos[i])]
    palabraNueva = palabraEntera.replace(letra, "")
    #si luego de eliminar la palabra el len queda en 0 de esa palabra en ese indicie de tiraTotalCartuchos la elimina
    if len(palabraNueva)==0:
        del tiraDeCartuchos[i]
    else:
        #guarda la nueva palabra formada la eliminacion de la letra encontrada dentro de la misma
        tiraDeCartuchos[i]=tiraDeCartuchos[i][0:13]+palabraNueva
    return True

#sistema de puntaje
def esCorrecta2(acerto,correctas):
    if acerto==True:
        puntitos=puntos2(correctas)
    else:
        puntitos=puntos2(-1)
    return puntitos

def puntos2(n):
    valor=0
    if n >= 0 and n < 15:
        segB1()
        valor=valor+0.1
    elif n >= 15:
        segB2()
        valor=valor+0.1*n#es mejor este tipo de matematica dentro del juego ya que no es facil llegar a los 15 y luego mantenerlo y el valor por seguidilla es mucho mas
                            #igualitario respecto al otro modo de juego, respecto al puntaje
    else:
        segM()
        valor=-0.1
    return valor


#Factores palabras volando
#lo que hace que la palabra se aleje mas del centro tanto hacia arriba como hacia abajo, y la sensacion de velocidad sobre el eje Z simulado por variacion de tamaño
def posicionXCart(valor):
    if valor >= 512:
        valor+=1
    else:
        valor-=1
    return valor
def posicionYCart(valor):
    if valor >= 300:
        valor+=1
    else:
        valor-=1
    return valor
def tamannioPalabra(valor):
    valor+=1
    return valor


